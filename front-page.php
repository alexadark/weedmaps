<?php
add_action( 'beans_header_after_markup', 'wst_display_search_form' );
beans_remove_action( 'beans_post_content' );
add_action( 'beans_post_body_after_markup', 'wst_display_page_description' );

add_action( 'beans_post_body_after_markup', 'wst_display_learn_tiles' );

add_action( 'beans_post_body_after_markup', 'wst_display_featured_posts' );
function wst_display_featured_posts() {
	$context          = Timber::get_context();
	$args             = array(
		'post_type'      => 'post',
		'posts_per_page' => - 1,
		'category_name'  => 'introduction',
		'orderby'        => 'menu_order',
		'order'          => 'DESC'
	);
	$context['posts'] = Timber::get_posts( $args );
	$templates        = array( 'featured-posts.twig' );
	Timber::render( $templates, $context );
}

add_action( 'beans_footer_wrapper_before_markup', 'wst_display_get_the_app' );
beans_load_document();