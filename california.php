<?php
//Template name: California page
//Template Post Type: post

// Remove layout control.
add_filter( 'beans_layout', '__return_false' );

// Remove all main div and their hooks.
beans_remove_markup( 'beans_fixed_wrap[_main]', true );
beans_remove_markup( 'beans_main_grid', true );
beans_remove_markup( 'beans_primary', true );

// Adjust attributes.
beans_remove_attribute( 'beans_main', 'class', 'uk-block' );


beans_modify_action_callback( 'beans_loop_template', 'wst_basic_loop' );


function wst_basic_loop() {

	if ( have_posts() ) :

		while ( have_posts() ) : the_post();

			the_content();

		endwhile;

	endif;

}

remove_action( 'beans_header_after_markup', 'wst_display_hero_area' );


add_action( 'beans_content_after_markup', 'wst_display_california_hero' );
function wst_display_california_hero() {
	$context   = Timber::get_context();
	$templates = array( 'california-hero.twig' );
	Timber::render( $templates, $context );
}

add_action( 'beans_content_after_markup', 'wst_display_black_user_tiles' );
function wst_display_black_user_tiles() {
	$context   = Timber::get_context();
	$templates = array( 'black-user-tiles.twig' );
	Timber::render( $templates, $context );
}

add_action( 'beans_content_after_markup', 'wst_display_purchase_section' );
function wst_display_purchase_section() {
	$context   = Timber::get_context();
	$templates = array( 'purchase-section.twig' );
	Timber::render( $templates, $context );
}

add_action( 'beans_content_after_markup', 'wst_display_consume_section' );
function wst_display_consume_section() {
	$context   = Timber::get_context();
	$templates = array( 'consume-section.twig' );
	Timber::render( $templates, $context );
}
add_action( 'beans_content_after_markup', 'wst_display_faq_section' );
function wst_display_faq_section() {
	$context   = Timber::get_context();
	$templates = array( 'faq-section.twig' );
	Timber::render( $templates, $context );
}
add_action( 'beans_content_after_markup', 'wst_display_medical_section' );
function wst_display_medical_section() {
	$context   = Timber::get_context();
	$templates = array( 'medical-section.twig' );
	Timber::render( $templates, $context );
}
beans_load_document();