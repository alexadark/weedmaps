<?php
//Template name: view All page
beans_modify_action_callback( 'beans_loop_template', 'wst_display_view_all_loop' );

function wst_display_view_all_loop() {
	$context   = Timber::get_context();
	$testing_args  = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'category_name'=> 'lab-testing'
	);
	$product_args  = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'category_name'=> 'products-and-how-to-consume'
	);
	$body_args  = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'category_name'=> 'cannabis-and-your-body'
	);
	$history_args  = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'category_name'=> 'cannabis-and-its-evolution'
	);
	$plant_args  = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'category_name'=> 'the-plant'
	);
	$us_args  = array(
		'post_type' => 'laws',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'regions',
				'field' => 'slug',
				'terms'    => array( 'others', 'canada' ),
				'operator' => 'NOT IN'
			)
		),

	);
	$can_args  = array(
		'post_type' => 'laws',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'regions',
				'field' => 'slug',
				'terms'    => array( 'canada' ),
				'operator' => 'IN'

			)
		)
	);
	$other_args  = array(
		'post_type' => 'laws',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'regions',
				'field' => 'slug',
				'terms'    => array( 'others' ),
				'operator' => 'IN'

			)
		)
	);
	$context['testing_posts'] = Timber::get_posts( $testing_args );
	$context['product_posts'] = Timber::get_posts( $product_args );
	$context['body_posts'] = Timber::get_posts( $body_args );
	$context['history_posts'] = Timber::get_posts( $history_args );
	$context['plant_posts'] = Timber::get_posts( $plant_args );
	$context['countries'] = Timber::get_posts( $other_args );
	$context['canada_states'] = Timber::get_posts( $can_args );
	$context['us_states'] = Timber::get_posts( $us_args );
	$templates = array( 'view-all.twig' );
	Timber::render( $templates, $context );


}


beans_load_document();