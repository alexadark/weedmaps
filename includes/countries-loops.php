<?php
$us_args  = array(
	'post_type' => 'laws',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'regions',
			'field' => 'slug',
			'terms'    => array( 'others', 'canada' ),
			'operator' => 'NOT IN'
		)
	),

);
$can_args  = array(
	'post_type' => 'laws',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'regions',
			'field' => 'slug',
			'terms'    => array( 'canada' ),
			'operator' => 'IN'

		)
	)
);
$other_args  = array(
	'post_type' => 'laws',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'regions',
			'field' => 'slug',
			'terms'    => array( 'others' ),
			'operator' => 'IN'

		)
	)
);
$context['countries'] = Timber::get_posts( $other_args );
$context['canada_states'] = Timber::get_posts( $can_args );
$context['us_states'] = Timber::get_posts( $us_args );