<?php
beans_remove_markup( 'beans_archive_title' );
beans_remove_output( 'beans_archive_title_text' );
beans_remove_action( 'beans_post_meta' );
beans_remove_action( 'beans_post_meta_categories' );
beans_remove_action( 'beans_post_content' );
beans_remove_action( 'beans_post_image' );
beans_remove_action( 'beans_post_title' );


beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-width-medium-1-3' );
beans_add_attribute( 'beans_content', 'data-uk-grid-match', '{target: \'.learn-content\'}' );

beans_replace_attribute( 'beans_post', 'class', 'uk-article', 'learn-item' );
add_action( 'beans_post_prepend_markup', 'wst_display_learn_item_content' );
function wst_display_learn_item_content() {
	$context   = Timber::get_context();
	$templates = array( 'includes/learn-item-content.twig' );
	Timber::render( $templates, $context );
}

add_action( 'beans_content_after_markup', 'wst_display_learn_tiles' );
add_action( 'beans_footer_wrapper_before_markup', 'wst_display_get_the_app' );