<?php
add_action( 'beans_header_after_markup', 'wst_display_search_form' );
add_action( 'beans_content_before_markup', 'wst_display_search_title' );
function wst_display_search_title(){
    global $wp_query;
    printf('<h2 class="search-result">%s Results for "%s"</h2>', $wp_query->found_posts, $_GET['s']);
}
$searchpage = true;
if($wp_query->found_posts){
    include_once 'includes/posts-grid.php';
} else {
    beans_remove_output('beans_search_title_text');
    beans_add_attribute('beans_search_title', 'class', 'hide');
    beans_add_attribute('beans_post', 'class', 'hide');
    add_action('beans_content_append_markup', 'wst_show_custom_text');

}
function wst_show_custom_text(){
?>
    <div class="not-found-text">
        Sorry, no topics were found. Please try searching for something else
    </div>
<?php
}
beans_load_document();