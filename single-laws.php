<?php

include_once 'includes/remove-loop-content.php';

add_action( 'beans_post_prepend_markup', 'wst_display_single_law' );
function wst_display_single_law() {
	$context   = Timber::get_context();
	include_once 'includes/countries-loops.php';
	$templates = array( 'single-law.twig' );
	Timber::render( $templates, $context );


}


beans_load_document();