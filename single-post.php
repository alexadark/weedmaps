<?php




//meta
beans_remove_action( 'beans_post_meta' );
beans_remove_action( 'beans_post_meta_categories' );
beans_remove_action( 'beans_post_navigation' );
$categories = get_the_category();
$cat_slug   = $categories[0]->slug;
if($cat_slug === 'products-and-how-to-consume'){
	include_once 'products.php';
} else {

	add_action( 'beans_post_title_after_markup', 'wst_display_cat', 1 );

	function wst_display_cat() {
		$categories = get_the_category();
		$cat_slug   = $categories[0]->slug;
		$cat_name   = $categories[0]->name;
		?>
        <div class="learn-cat <?php esc_html_e( $cat_slug ); ?>">
            <a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ) ?>"><?php esc_html_e( $cat_name ); ?></a>
        </div>
	<?php }

	add_action( 'beans_post_content_after_markup', 'wst_display_tags_and_date' );
	function wst_display_tags_and_date() { ?>
        <div class="post-tags"><?php the_tags( '', '', '' ); ?></div>
        <div class="post-date"><?php the_date(); ?></div>

	<?php }

	add_action( 'beans_main_after_markup', 'wst_display_related_and_learn_tiles' );
	function wst_display_related_and_learn_tiles() { ?>
        <div class="single-post-bottom">
            <div class="uk-container uk-container-center">


				<?php
				$context   = Timber::get_context();
				$templates = array( 'related-topics.twig' );
				Timber::render( $templates, $context );


				wst_display_learn_tiles();
				?>
            </div>
        </div>

	<?php }


	add_action( 'beans_footer_wrapper_before_markup', 'wst_display_get_the_app' );
}

beans_load_document();