<?php
//Template name: Law Regulation
beans_modify_action_callback( 'beans_loop_template', 'wst_display_law_loop' );


function wst_display_law_loop() {

	$context = Timber::get_context();

	include_once 'includes/countries-loops.php';
    $context['img_src'] = CHILD_URL.'/assets/images/';
	$templates            = array( 'law.twig' );
	Timber::render( $templates, $context );

}


beans_load_document();