<?php


beans_modify_action_callback( 'beans_loop_template', 'wst_display_product_loop' );

function wst_display_product_loop() {
	$context   = Timber::get_context();
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'category_name'=> 'products-and-how-to-consume',
		'post__not_in' => array(get_the_ID()),
		'orderby'=>'menu_order',
		'order'=> 'ASC'
	);
	$context['prod_posts'] = Timber::get_posts($args);
	$templates = array( 'product.twig' );
	Timber::render( $templates, $context );


}