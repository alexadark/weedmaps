<?php
//Template name: Dictionary
beans_modify_action_callback( 'beans_loop_template', 'wst_display_dictionary_loop' );


function wst_display_dictionary_loop() {

	$context = Timber::get_context();
	$ac_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'a-c' ),
				'operator' => 'IN'
			)
		),

	);
	$df_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'd-f' ),
				'operator' => 'IN'
			)
		),

	);
	$gi_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'g-i' ),
				'operator' => 'IN'
			)
		),

	);
	$jl_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'j-l' ),
				'operator' => 'IN'
			)
		),

	);
	$mo_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'm-o' ),
				'operator' => 'IN'
			)
		),

	);
	$pr_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'p-r' ),
				'operator' => 'IN'
			)
		),

	);
	$su_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 's-u' ),
				'operator' => 'IN'
			)
		),

	);
	$vz_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'v-z' ),
				'operator' => 'IN'
			)
		),

	);
	$numbers_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'number' ),
				'operator' => 'IN'
			)
		),

	);

	$context['vz_range'] = Timber::get_posts( $vz_args );
	$context['su_range'] = Timber::get_posts( $su_args );
	$context['pr_range'] = Timber::get_posts( $pr_args );
	$context['mo_range'] = Timber::get_posts( $mo_args );
	$context['jl_range'] = Timber::get_posts( $jl_args );
	$context['gi_range'] = Timber::get_posts( $gi_args );
	$context['df_range'] = Timber::get_posts( $df_args );
	$context['ac_range'] = Timber::get_posts( $ac_args );
	$context['numbers'] = Timber::get_posts( $numbers_args );
	$templates            = array( 'dictionary.twig' );
	Timber::render( $templates, $context );

}


beans_load_document();