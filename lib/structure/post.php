<?php

add_action( 'wp', 'wst_set_up_post_structure' );
/**
 * Set up post structure
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_set_up_post_structure() {
	//Remove featured image (it will appear in hero area)
	beans_remove_action( 'beans_post_image' );

	//comments
	remove_post_type_support( 'post', 'comments' );
	remove_post_type_support( 'page', 'comments' );



	//pages
	if ( ! is_page() ) {
		return;
	}
	//Remove title only on pages
	beans_remove_action( 'beans_post_title' );



}

function wst_display_page_description() { ?>
    <div class="page-description uk-text-center">
        <?php
        if(is_front_page()) {
	        the_content();
        } else {
            echo category_description();
        }

        ?>
    </div>
<?php }


function wst_display_get_the_app() {

		$context   = Timber::get_context();
		$templates = array( 'get-the-app.twig' );
		Timber::render( $templates, $context );

}

