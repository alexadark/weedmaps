<?php
add_action( 'wp', 'wst_set_up_footer_structure' );
/**
 * Set up footer structure
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_set_up_footer_structure() {
	beans_wrap_markup( 'beans_footer', 'beans_footer_wrapper', 'div', array( 'class' => 'tm-footer-wrapper' ) );

	beans_remove_attribute('beans_widget_panel','class','uk-panel-box');

	//FAT FOOTER
	beans_add_smart_action( 'beans_footer_wrapper_prepend_markup', 'wst_display_fat_footer' );

}

function wst_display_fat_footer() {
	if(!is_active_sidebar('fat-footer')){
		return;
	}?>
    <div class="tm-fat-footer uk-block">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-medium-4-10"><?php echo beans_widget_area( 'fat-footer-left' ); ?></div>
                <div class="uk-width-medium-6-10 accordion-footer"><?php echo beans_widget_area( 'fat-footer-right' ); ?></div>
            </div>
        </div>
    </div>

<?php }

// Overwrite the Footer content

beans_modify_action_callback( 'beans_footer_content', 'beans_child_footer_content' );

function beans_child_footer_content() {
	$context                = Timber::get_context(); //what is default role of this template ?
	$context['year']        = date( 'Y' );
    $context['small_logo']  = CHILD_URL.'/assets/images/small-logo.svg';
    $context['header_logo']  = CHILD_URL.'/assets/images/logo-header.svg';
	$templates              = array( 'footer.twig' );
	Timber::render( $templates, $context ); //make ingredients ($context) available to this template
}

beans_add_attribute('beans_widget_content', 'class', 'slide-content');

add_action('beans_widget_title[_fat-footer-right]_append_markup', 'set_plus_icon');
function set_plus_icon(){
    ?>
    <i class="uk-icon-plus"></i>
    <i class="uk-icon-minus"></i>
    <?php
}