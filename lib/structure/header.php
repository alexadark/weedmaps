<?php
add_action( 'wp', 'wst_set_up_header_structure' );
/**
 * Set up header structure
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_set_up_header_structure() {
    //change logo link
    beans_replace_attribute('beans_site_title_link','href', 'https://weedmaps.com/');

	//Remove title tagline
	beans_remove_action( 'beans_site_title_tag' );

	//sticky header
	beans_add_attribute( 'beans_header', 'data-uk-sticky', "{top:-300, animation:'uk-animation-slide-top', media: 768}" );

	//Hero area

	add_action( 'beans_header_after_markup', 'wst_display_hero_area' );


	// Breadcrumb
	beans_remove_action( 'beans_breadcrumb' );

	//top bar
	add_action( 'beans_header_before_markup', 'wst_display_top_bar' );
	beans_remove_attribute('beans_widget_panel_top-bar','class','uk-panel-box');
	beans_add_attribute('beans_widget_panel_top-bar','class','uk-hidden-small');

}

/**
 * Display top bar
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_display_top_bar(){

	if(!is_active_sidebar('top-bar')){
		return;
	} ?>

    <div class="top-bar">
        <div class="uk-container uk-container-center">
			<?php echo beans_widget_area( 'top-bar' ); ?>
        </div>
    </div>

<?php }

/**
 * Display hero area
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_display_hero_area() {

	if(is_home()){
		return;
	}
	$context   = Timber::get_context();
	$templates = array( 'hero.twig' );
	if(is_archive()){
		$cat = get_queried_object();
		$context['background'] = get_field('category_hero_background', $cat);
		$context['icon'] = get_field('category_icon', $cat);
		$templates = array( 'hero-archive.twig');
	}
	if(is_search()){
		$templates = array( 'hero-search.twig');
	}
	Timber::render( $templates, $context );


}

/*
 * Display mobile site logo
 * */

add_action('beans_site_title_link_append_markup', 'wst_set_mobile_header_logo_func');
function wst_set_mobile_header_logo_func(){
	$image_url = CHILD_URL . '/assets/images/header-mobile-logo.png'
	?>
    <img class="header-logo-mobile" src="<?php echo $image_url; ?>" alt="">
	<?php
}

beans_add_attribute('beans_search_form', 'id', 'search-form');

add_action('beans_primary_menu_append_markup', 'wst_set_search_icon_func');
function wst_set_search_icon_func(){
	?>
    <a href="#" class="mobile-search-icon"><i class="uk-icon-search"></i></a>
	<?php
}

add_action('beans_header_prepend_markup', 'wst_mobile_search_form');
function wst_mobile_search_form(){
	?>
    <div class="mobile-search-menu">
        <div class="padder">
            <form action="" method="get">
                <i class="uk-icon-search"></i>
                <input type="text" class="search-input" name="s" placeholder="What would you like to learn about?">
                <a href="#" class="cancel">Cancel</a>
            </form>
        </div>
    </div>
	<?php
}