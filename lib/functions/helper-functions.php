<?php
/**
 * Give a body class 'has-featured-image' when featured image exists
 *
 * @since 1.0.0
 *
 * @param $classes
 *
 * @return array
 */
function wst_add_featured_image_body_class( $classes ) {

	global $post;
	if ( isset ( $post->ID ) && get_the_post_thumbnail($post->ID) && !is_home() && !is_archive()) {
		$classes[] = 'has-featured-image';
	}
	return $classes;
}
add_filter( 'body_class', 'wst_add_featured_image_body_class' );

function wst_display_learn_tiles() {
	$context   = Timber::get_context();
	$templates = array( 'learn-tiles.twig' );
	Timber::render( $templates, $context );
}

function wst_display_search_form(){
	print('<div class="block-search">');
	$value = isset($_GET['s'])? $_GET['s']:'';
	?>
    <form class="uk-form uk-form-icon uk-form-icon-flip uk-width-1-1" method="get" action="" role="search" id="search-form" data-markup-id="beans_search_form"><input class="uk-width-1-1" type="search" placeholder="What would you like to learn about?" value="<?php echo $value; ?>" name="s" data-markup-id="beans_search_form_input"><i class="uk-icon-search" data-markup-id="beans_search_form_input_icon"></i></form>
    <?php
	print('</div>');
}

function remove_pages_from_search($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
add_filter('pre_get_posts','remove_pages_from_search');


add_filter('body_class','add_category_to_single');
function add_category_to_single($classes) {
	if (is_single() ) {
		global $post;
		foreach((get_the_category($post->ID)) as $category) {
			// add category slug to the $classes array
			$classes[] = $category->category_nicename;
		}
	}
	// return the $classes array
	return $classes;
}