<?php
add_action( 'widgets_init', 'wst_register_widget_areas' );
/**
 * Register your widget areas here
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_register_widget_areas() {

	$widgets_areas = array(

		array(
			'name'        => __( 'Fat Footer left', CHILD_TEXT_DOMAIN ),
			'id'          => 'fat-footer-left',
			'beans_type'  => 'stack',
			'description' => __( 'This is the widget area for the fat footer left widgets', CHILD_TEXT_DOMAIN )
		),
		array(
			'name'        => __( 'Fat Footer right', CHILD_TEXT_DOMAIN ),
			'id'          => 'fat-footer-right',
			'beans_type'  => 'grid',
			'description' => __( 'This is the widget area for the fat footer right widgets', CHILD_TEXT_DOMAIN )
		),
		array(
			'name'        => __( 'Top Bar', CHILD_TEXT_DOMAIN ),
			'id'          => 'top-bar',
			'beans_type'  => 'grid',
			'description' => __( 'This is the widget area for the top bar', CHILD_TEXT_DOMAIN )
		),
	);

	foreach ( $widgets_areas as $widget_area ) {

		beans_register_widget_area( $widget_area );


	}
}

