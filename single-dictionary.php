<?php
include_once 'includes/remove-loop-content.php';
add_action( 'beans_post_prepend_markup', 'wst_display_single_dictionary' );
function wst_display_single_dictionary() {

	$args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'number' ),
				'operator' => 'NOT IN'
			)
		),

	);
	$number_args  = array(
		'post_type' => 'dictionary',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'letters',
				'field' => 'slug',
				'terms'    => array( 'number' ),
				'operator' => 'IN'
			)
		),

	);
	$context   = Timber::get_context();
	$context['letter_range'] = Timber::get_posts( $args );
	$context['numbers'] = Timber::get_posts($number_args);
	$templates = array( 'single-dictionary.twig' );
	Timber::render( $templates, $context );


}

beans_load_document();