
(function ($) {
    $(document).ready(function () {
        //js code here
        $('.tm-widget.widget_nav_menu .uk-panel-title').click(function(){
            //$('.tm-widget.widget_nav_menu .slide-content').slideUp();
            if($(window).width() < 960){
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            }
        });
        $('.mobile-search-icon').click(function(){
            $('.mobile-search-menu').slideToggle();
            $(this).find('input').focus();
        });
        $('.cancel').click(function(){
            $('.mobile-search-menu').slideToggle();
        });

        //load posts for view all page
        function loadPostsTitles(id,url) {
            var introList = $(id);
            introList.load(url + " .learn-title a");
            $(document).ajaxComplete(function () {
                introList.find('a')
                    .wrap('<li></li>');
            });
        }
        // loadPostsTitles('#intro','/');
        // loadPostsTitles('#testing','/cannabis-and-your-body');



        function heightsEqualizer(selector) {
            var elements = document.querySelectorAll(selector),
                max_height = 0,
                len = 0,
                i;

            if ( (elements) && (elements.length > 0) ) {
                len = elements.length;

                for (i = 0; i < len; i++) { // get max height
                    elements[i].style.height = ''; // reset height attr
                    if (elements[i].clientHeight > max_height) {
                        max_height = elements[i].clientHeight;
                    }
                }

                for (i = 0; i < len; i++) { // set max height to all elements
                    elements[i].style.height = max_height + 'px';
                }
            }
        }
        heightsEqualizer('.related-topics-grid .title');
    });
})(jQuery);